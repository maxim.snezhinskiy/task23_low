﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Low.Controllers
{
    public class GuestController : Controller
    {
        static List<(string name, DateTime date, string comment)> comments = new List<(string name, DateTime date, string comment)>
            {
                ("Mike Brown", new DateTime(2005, 5, 8) , "Not bad"),
                ("John Williams", new DateTime(2020, 4, 17), ".NET is the best platform"),
                ("Jack Daniels", new DateTime(2021, 7, 3) , "I hate JS"),
            };

        [HttpGet]
        public ActionResult FeedBack()
        { 
           
            ViewBag.FeedBack = comments;
            return View();
        }

        [HttpPost]
        public ActionResult FeedBack(FormCollection form)
        {
            comments?.Add((form["Author"], DateTime.Now, form["Comment"]));
            ViewBag.FeedBack = comments;
            return View();
        }

  
    }
}