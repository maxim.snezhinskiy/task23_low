﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Low.Controllers
{
    public class QuestionaryController : Controller
    {


        [HttpGet]
        public ActionResult Questionary()
        {
            var mainLang = new List<string>()
            {
                "C#",
                "Java",
                "JavaSript",
            };

            var lang = new List<string>()
            {
                "English",
                "Ukrainian",
                "Other"
            };

            ViewBag.LangQuiz = lang;
            ViewBag.MainLang = mainLang;
            return View();
        }

        [HttpPost]
        public ActionResult Questionary(string LangQuiz, bool second, string MainLang, string EmailInput)
        { 
            return RedirectToAction("QuestionaryResult", new {lang = LangQuiz, likeProgramming = second, mainLang=  MainLang, email = EmailInput });
        }

        public ActionResult QuestionaryResult(string lang, bool likeProgramming, string mainLang, string email)
        {
            ViewBag.Lang = lang;
            ViewBag.LikeProgramming = likeProgramming ? "Yes": "No";
            ViewBag.MainLang = mainLang;
            ViewBag.Email = email;
            return View();
        }
    }
}