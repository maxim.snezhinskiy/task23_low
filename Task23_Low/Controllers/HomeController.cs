﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Low.Controllers
{
    public class HomeController : Controller
    { 

        private List<(string, DateTime, string)> GetArticles()
        {
            return new List<(string, DateTime, string)> {
                (".NET", DateTime.Now, $".NET is a free, cross-platform, open source developer platform for building many different types of applications." +
                $"With .NET, you can use multiple languages, editors, and libraries to build for web, mobile, desktop, games, and IoT."),
                ("Frontend", new DateTime(2015, 5, 25), $"Front-end web development is the practice of converting data to a graphical interface, " +
                $"through the use of HTML, CSS, and JavaScript, so that users can view and interact with that data. "),
                ("Machine Learning", new DateTime(2004, 3, 2), $"Machine learning -Machine learning and data mining. v. t. e. Machine learning (ML)" +
                $" is the study of computer algorithms that improve automatically through experience and by the use of data. It is seen as a part of artificial intelligence." +
                $" Machine learning algorithms build a model based on sample data, known as \"training data\". ")
            };
        }

        public ActionResult Index()
        {

            ViewBag.Articles = GetArticles() ;
            return View();
        }

    }
}